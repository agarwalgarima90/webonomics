/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var module = angular.module("webonomics", []);

//Service for Managing the Cart Products
module.service("manageCartOperations", function () {
	this.buyProducts = [
		{image: "",
			name: "",
			price: ""
		}
	];

	//create
	this.addProduct = function (newimage, newname, newprice) {
		this.buyProducts.push({image: newimage, name: newname, price: newprice});
	};

	//retrive
	this.getProducts = function () {
		return this.buyProducts;
	};

	//delete
	this.deleteProduct = function (delname) {
		for (var i = 0; i < this.buyProducts.length; i++) {
			if (this.buyProducts[i].name == delname) {
				this.buyProducts.splice(i, 1);
			}
		}
	};

});

module.controller("paymentController", function($scope, $http, $rootScope) {
	
	$scope.creditCard = false;
	$scope.selectedBank = "";
	$scope.months = ['1','2','3','4','5','6','7','8','9','10','11','12'];
	$scope.years = ['2016','2017','2018','2019','2020','2021','2022','2023','2024'];
	$scope.banks = ['Axiz Bank', 'HDFC Bank', 'ICICI Bank', 'State Bank of Inidia', 'Kotak Bank','xiz Bank', 'DFC Bank', 'ICII Bank', 'Stae Bank of Inidia', 'otak Bank','Axiz ank', 'HC Bank', 'ICICI Bnk', 'State Bank of Iidia', 'Kotak Bak'];
	$scope.debitCards = ['All Visa/MasterCard Debit Card', 'All SBI Maestro Debit Cards', 'Citibank Maestro Debit Card', 'All Rupay Debit Cards', 'ICICI Debit Card with ATm Pin'];
});

module.controller("productsController", function (manageCartOperations, $scope, $http, $rootScope) {
	
	$rootScope.Products = []; //Array for storing the data fetched from JSON file
	$scope.productBrands = []; //Aray to store the available brands.
	$scope.count = 8; //To set the initial number of records to be displayed
	
	$scope.productName = "";
	$scope.currentUser = "Guest";
	$scope.currentCategory = "";
	$scope.order = 'prod_name';
	$scope.priceFilterValueMin = 100;
	$scope.priceFilterValueMax;
	$scope.mainImage = "";

	$scope.test = function() {
		console.log(sessionStorage.getItem("finalCart"));
		console.log(localStorage.getItem("name"));
		sessionStorage.setItem("test", "hello");
		console.log(sessionStorage.getItem("test"));
	};
	
	//init function which will be called as the div loads
	$scope.init = function() {
		
		$scope.currentCategory = sessionStorage.getItem("currentCategory");
		$scope.currentUser = sessionStorage.getItem("currentUser");
		
		if($scope.currentCategory == null) {
			alert("Category not found. all products should e loaded");
			
		}
		
		var jsondata = $http.get("http://localhost/crestrou/Webonomics_Angular/getItems.php",
					{params: {"currentCategory" : $scope.currentCategory}});
		jsondata.success(function (data) {
			$rootScope.Products = data;
		});
		jsondata.error(function (error) {
			console.log("Error: " + error);
		});
		
		//To get the brands
		var brands = $http.get("http://localhost/crestrou/Webonomics_Angular/getBrands.php",
					{params: {"currentCategory" : $scope.currentCategory}});
		brands.success(function(data) {
			$scope.productBrands = data;
			console.log($scope.productBrands);
		});
		brands.error(function(error) {
			console.log("Error: " + error);
		});
		
		//To get the maximum available price for filtering
		var maxPrice = $http.get("http://localhost/crestrou/Webonomics_Angular/getMaxPrice.php",
					{params: {"currentCategory" : $scope.currentCategory}});
		maxPrice.success(function(data) {
			$scope.priceFilterValueMax = data;
			console.log($scope.currentCategory);
			console.log($scope.priceFilterValueMax[0].prod_price);
		});
		brands.error(function(error) {
			console.log("Error: " + error);
		});
	};
	
	$scope.cartProducts = [];
	$scope.totalPrice = 0;
	$scope.currentNumberOfItems = 0;
	
	//Function to retrive products
	$scope.GetProducts = function() {
		$scope.cartProducts = manageCartOperations.getProducts();
	};
	//Function to add the product to cart
	$scope.addToCart = function(newimage, newname, newprice) {
		manageCartOperations.addProduct(newimage, newname, newprice);
		$scope.totalPrice = $scope.totalPrice + newprice;
		$scope.currentNumberOfItems += 1;
		alert("Product has been added to your cart");
	};
	
	$scope.delFromCart = function(delproduct, delprice) {
		manageCartOperations.deleteProduct(delproduct);
		$scope.totalPrice = $scope.totalPrice - delprice;
		$scope.currentNumberOfItems -= 1;
	}

	//Function for checkout
	$scope.checkout = function() {
		if ($scope.cartProducts.length <= 1) {
			alert("Cart is empty");
		}
		else {
			$scope.inCart = $scope.cartProducts.length -1;
			alert("Your cart has " + $scope.inCart  + " products.");
			sessionStorage.setItem("finalCart", JSON.stringify($scope.cartProducts));
			localStorage.setItem("name", "garima");
			window.location.href = "http://192.168.0.111/crestrou/Webonomics_Angular/payment.html";
		}
	}

	//Function to change the main image based on small image selection
	$scope.changeImage = function(imageUrl) {
		console.log("ImageUrl received: " + imageUrl);
		$scope.mainImage = imageUrl;
		console.log("MainImageUrl: " + $scope.mainImage);
	};
	
	//Function called when delete is clicked to update the main image for once
	$scope.updateMainImage = function(imageUrl) {
		$scope.mainImage = imageUrl;
	}
	

	//Custom filter for Price
	$scope.PriceFilter = function(Products) {
		if(Products.prod_price < $scope.priceFilterValueMin) {
			return;
		}
		return Products;
	};
	
	//function to populate values to be used in brand custom filter
	$scope.brandIncludes = [];
	$scope.includeBrand = function(brand) {
		var i = $.inArray(brand, $scope.brandIncludes);
		if(i > -1) {
			$scope.brandIncludes.splice(i,1);
		} else {
			$scope.brandIncludes.push(brand);
		}
	};
	
	//Custom filter for brand
	$scope.BrandFilter = function(Products) {
		if($scope.brandIncludes.length > 0) {
			if($.inArray(Products.prod_seller, $scope.brandIncludes) < 0) {
				return;
			}
		}
		return Products;
	};
	
	//Function to set the sorting order
	$scope.setOrder = function(order) {
		$scope.order = order;
	};
	
	//Function to load more products
	$scope.ShowMore = function () {
		if ($scope.count > $scope.Products.length) {
			alert("No more records Found");
		}
		$scope.count = $scope.count + 8;
	};
	
	//Custom filter to serach on the bases of name
	$scope.FilterProducts = function (p) {
		return p.prod_name.substring(0, $scope.productName.length) == $scope.productName;
	};
	
});

module.config(function($httpProvider) {
	//Enable cross-domain calls
	$httpProvider.defaults.useXDomain = true;
	
	//Remove the header  used to identify ajax call that would prevent CORS from working
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
});


