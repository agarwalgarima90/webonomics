/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var module = angular.module("webonomics", ['ngRoute']);


module.controller("WebonomicsController", function ($scope, $http, $rootScope, $window) {
	$rootScope.ViewName = 'login';
	
	$scope.form = {
		loginusername : "",
		loginpassword : "",
		registerusername : "",
		registerpassword : ""
	};
	
	$scope.ShowPwd = false;
	$scope.isLogged = false;
	
//	Function to validate the user input and login successfully
	$scope.logInUser = function() {
		
		$scope.notFound = "00000";
		
		console.log("Username: " + $scope.form.loginusername);
		
		var jsondata = $http.get("http://localhost/crestrou/Webonomics_Angular/login.php", 
					{params: {"uname" : $scope.form.loginusername, 
						"password" : $scope.form.loginpassword}});
		jsondata.success(function (data) {
			if(data == $scope.notFound ) {
				alert("Enteres user is not registered")
			} else {
				$scope.isLogged = true;
				$rootScope.loggedInUser = $scope.form.loginusername;
				sessionStorage.setItem("currentUser", $scope.form.loginusername);
				sessionStorage.setItem("isLogged", $scope.isLogged);
				$rootScope.ViewName = 'main';
				$window.location.href = 'main.html';
			}

		});
		jsondata.error(function (error) {
			console.log("Error Caused: " + error);
		});
	};
	
	//	Function to validate the user input and Signup
	$scope.signUpUser = function() {
		
		$scope.alreadyExist = "23000";
		
		console.log("Username: " + $scope.form.registerusername);
		var jsondata = $http.get("http://localhost/crestrou/Webonomics_Angular/signup.php", 
					{params: {"uname" : $scope.form.registerusername, 
						"password" : $scope.form.registerpassword}});
		jsondata.success(function (data) {
			console.log("Data: " + data);
			if(data == $scope.alreadyExist ) {
				alert("Username already exists");
			} else {
				alert("Registered Successfully");
				$rootScope.isLogged = true;
				$rootScope.loggedInUser = $scope.form.registerusername;
				if ($rootScope.isLogged == true) {
					$window.location.href = 'main.html';
				}
			}
		});
		jsondata.error(function (error) {
			console.log("Error Caused: " + error);
		});
	};
});

module.controller("mainpageController", function ($scope, $rootScope, $http) {
	$rootScope.loggedInUser = "Guest User";
	$scope.isLogged = false;
	$scope.categories = [];
	$scope.currentCategory = "";
	$scope.MyData = [];
	
	//init function which will be called as the div loads
	$scope.init = function() {
		$scope.isLogged = false;
		$rootScope.loggedInUser = sessionStorage.getItem("currentUser");
		$scope.isLogged = sessionStorage.getItem("isLogged");
		
		if ($scope.isLogged) {
//			var jsondata = $http.get("http://localhost/crestrou/Webonomics_Angular/getCategory.php");
			var jsondata = $http.get("http://localhost/crestrou/Webonomics_Angular/getCategory.php");
			jsondata.success(function (data) {
				$scope.categories = data;
			});
			jsondata.error(function (error) {
				console.log("Error: " + error);
			});

		}
		else {
			alert("Please Login First");
			window.location.href = "http://localhost/crestrou/Webonomics_Angular/index.html";
		}
	};
	
	$scope.logoutUser = function() {
		$scope.isLogged = false;
		sessionStorage.removeItem("isLogged");
		window.location.href = "http://localhost/crestrou/Webonomics_Angular/index.html"
	};
	
	$scope.displayItems = function(categoryName) {
		sessionStorage.setItem("currentCategory", categoryName);
		window.location.href = "http://localhost/crestrou/Webonomics_Angular/product.html";
	};
});



module.config(function($routeProvider) {
	//configure routes here
	$routeProvider.when('/faq', {
		templateUrl : 'faq.html'
	}).when('/feedback', {
		templateUrl : 'feedback.html'
	}).when('/', {
		templateUrl : 'login.html'
	}).when('/register', {
		templateUrl : 'register.html'
	}).when('/aboutus', {
		templateUrl : 'aboutus.html'
	}).when('/login', {
		templateUrl : 'login.html'
	});
});